package com.yalantis.secondproject;


public class ItemData {
    public String mDescription;
    public String mAddress;
    public String mData;
    public String mExpectation;
    public int mIconView;

    ItemData(String myDescription, String myAddress, String myData, String myExpectation, int myIconView){
        mDescription = myDescription;
        mAddress = myAddress;
        mData = myData;
        mExpectation = myExpectation;
        mIconView = myIconView;
    }
}

package com.yalantis.secondproject;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class Fragment_2 extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_2, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view_2);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        setItemData();

        return rootView;
    }

    private void setItemData(){
        ItemData[] mItemData= new ItemData[4];
        mItemData[0] = new ItemData("Демонтаж інших обектів, що входять до переліку мал...","Вул. Вадима Гетьмана 57","Кві 16, 2016","-1 днів",R.drawable.ic_trash);
        mItemData[1] = new ItemData("Благоустрій та будівництво","Вул. Вадима Гетьмана 7","Кві 16, 2016","-6 днів",R.drawable.ic_trash);
        mItemData[2] = new ItemData("Ремонт ліфтів","Вул. Вадима Гетьмана 577","Кві 16, 2016","-4 днів",R.drawable.ic_issues);
        mItemData[3] = new ItemData("Спортмайданчики","Вул. Вадима Гетьмана 111","Кві 16, 2016","-6 днів",R.drawable.ic_issues);

        mAdapter = new MyAdapterOne(mItemData);
        mRecyclerView.setAdapter(mAdapter);
    }

}

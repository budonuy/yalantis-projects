package com.yalantis.secondproject;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapterOne extends RecyclerView.Adapter<MyAdapterOne.ViewHolder> {
    private ItemData[] mItemData;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtDescription;
        public TextView txtAddress;
        public TextView txtData;
        public TextView txtExpectation;
        public ImageView IconView;

        public ViewHolder(View v) {
            super(v);
            txtDescription = (TextView) v.findViewById(R.id.txt_description);
            txtAddress = (TextView) v.findViewById(R.id.txt_address);
            txtData = (TextView) v.findViewById(R.id.txt_data);
            txtExpectation = (TextView) v.findViewById(R.id.txt_expectation);
            IconView = (ImageView) v.findViewById(R.id.icon_view);
        }
    }

    public MyAdapterOne(ItemData[] myItemData) {
        mItemData = myItemData;
    }

    @Override
    public MyAdapterOne.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtDescription.setText(mItemData[position].mDescription);
        holder.txtAddress.setText(mItemData[position].mAddress);
        holder.txtData.setText(mItemData[position].mData);
        holder.txtExpectation.setText(mItemData[position].mExpectation);
        holder.IconView.setImageResource(mItemData[position].mIconView);
    }

    @Override
    public int getItemCount() {
        return mItemData.length;
    }
}